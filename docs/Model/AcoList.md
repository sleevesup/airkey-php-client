# AcoList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**aco_list** | [**\Evva\AirKey\Model\Aco[]**](Aco.md) | list of acos | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


