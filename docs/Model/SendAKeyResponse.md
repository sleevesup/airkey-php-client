# SendAKeyResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phone** | [**\Evva\AirKey\Model\Phone**](Phone.md) | Phone (either looked up by the requested one or the created one) | [optional] 
**person** | [**\Evva\AirKey\Model\Person**](Person.md) | Same as in request, supplemented with default data / existing data and id | [optional] 
**authorization** | [**\Evva\AirKey\Model\Authorization**](Authorization.md) | Created/existing authorization | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


