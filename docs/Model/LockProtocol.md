# LockProtocol

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lock** | [**\Evva\AirKey\Model\SimpleLock**](SimpleLock.md) | Lock with which this protocol entry is associated | [optional] 
**lock_protocol** | [**\Evva\AirKey\Model\LockProtocolEntry**](LockProtocolEntry.md) | Protocol entry of a lock | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


