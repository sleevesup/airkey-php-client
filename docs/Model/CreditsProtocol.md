# CreditsProtocol

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**administrator_name** | **string** | Name of the administrator who was responsible for this event | [optional] 
**user_id** | **int** | Unique identifier of the administrator who was responsible for this event | [optional] 
**credits_protocol_entry** | [**\Evva\AirKey\Model\SystemProtocolEntry**](SystemProtocolEntry.md) | System protocol entry with event details | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


