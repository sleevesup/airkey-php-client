# PendingPhoneReplacementListDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pending_phone_replacement_list** | [**\Evva\AirKey\Model\PendingPhoneReplacementDto[]**](PendingPhoneReplacementDto.md) | List of pending phone replacements | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


