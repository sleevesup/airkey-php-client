# AuthorizationChange

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**authorization_create_list** | [**\Evva\AirKey\Model\AuthorizationCreate[]**](AuthorizationCreate.md) | List of medium authorizations to be created for the provided locks/areas | 
**authorization_update_list** | [**\Evva\AirKey\Model\Authorization[]**](Authorization.md) | List of medium authorizations to be updated | 
**push_message** | **string** | Notification text that will be shown on the phone for new/changed authorizations. Default text will be used if not set. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


