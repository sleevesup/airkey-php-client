<?php
/**
 * BlacklistsApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Evva\AirKey
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * EVVA AirKey Cloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v18.0.4
 * Contact: office-wien@evva.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.41
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Evva\AirKey;

use \Evva\AirKey\Configuration;
use \Evva\AirKey\ApiException;
use \Evva\AirKey\ObjectSerializer;

/**
 * BlacklistsApiTest Class Doc Comment
 *
 * @category Class
 * @package  Evva\AirKey
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class BlacklistsApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for getBlacklists
     *
     * Gets all available blacklist entries..
     *
     */
    public function testGetBlacklists()
    {
    }
}
